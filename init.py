import config
import time
from machine import Pin, Timer, I2C
from onewire import OneWire
from ds18x20 import DS18X20
from umqtt.simple import MQTTClient
from neopixel import NeoPixel
import ads1x15
from ssd1306 import SSD1306_I2C

print("Initalising")

############ Constants ############
HIGH=1
LOW=0

mqttClient = MQTTClient(config.clientType, "192.168.0.1")

TEMPERATURE_SAMPLE_RATE_MS = 3000
TEMP_TOPIC = b"techtalk/temp"
SHORT_LED_COUNT = 10.0
LED_MULTIPLIER = 3
i2c = I2C(scl=Pin(5), sda=Pin(4), freq=400000)
display = SSD1306_I2C(128, 64, i2c)

LED_PATTERN_TOPIC = b"techtalk/ledpattern"

#ADC
LONG_LED_COUNT=25
ADC_SAMPLE_RATE_MS = 100
gain = 1
addr = 72
rate = 4
ads = ads1x15.ADS1115(i2c, addr, gain)
v2mv=1000
count2Pos=0.9
posChannel=0
count2RGB=9.5
redChannel=1
greenChannel=2
blueChannel=3

patternId=0

pirPin = Pin(12, Pin.IN, None)
PIR_TOPIC = b"techtalk/pir"

MANUAL_TOPIC = b"techtalk/manual"
ledBluePin = Pin(13, Pin.OUT)
ledRedPin = Pin(14, Pin.OUT)

#Sample termperature
def temperatureSample(x):
	print("Starting sample")
	ds = DS18X20(OneWire(Pin(0)))
	roms = ds.scan()
	ds.convert_temp()
	time.sleep_ms(750)
	tempDegC = ds.read_temp(roms[0])
	print("Temp {:.1f} Deg C".format(tempDegC))
	mqttClient.connect()
	mqttClient.publish(TEMP_TOPIC, b"{:.1f}".format(tempDegC))
	mqttClient.disconnect()

#Sample ADC Pots
def adcSample(x):
	pos = ads.read(rate, posChannel)
	pos = (pos*count2Pos)/v2mv
	if pos>LONG_LED_COUNT:
		pos=LONG_LED_COUNT
	if pos<0:
		pos=0
	pos=int(pos)
	#print("Position {:.0f}".format(pos))

	red = ads.read(rate, redChannel)
	red = (red*count2RGB)/v2mv
	if red>256:
		red=256
	if red<0:
		red=0
	red=int(red)
	#print("Red {:.0f}".format(red))

	green = ads.read(rate, greenChannel)
	green = (green*count2RGB)/v2mv
	if green>256:
		green=256
	if green<0:
		green=0
	green=int(green)
	#print("Green {:.0f}".format(green))

	blue = ads.read(rate, blueChannel)
	blue = (blue*count2RGB)/v2mv
	if blue>256:
		blue=256
	if blue<0:
		blue=0
	blue=int(blue)
	#print("Blue {:.0f}".format(blue))

	#clear the rest
	for n in range(0, LONG_LED_COUNT):
		if n==(pos-1) or n==(pos+1):
			#print("Buffer Pos RGB {:.0f} {:.0f} {:.0f} {:.0f}".format(pos, int(red/5), int(green/5), int(blue/5)))
			neoPixel[n] = (int(red/5), int(green/5), int(blue/5))
		elif n==pos:
			#print("Main Pos RGB {:.0f} {:.0f} {:.0f} {:.0f}".format(pos, red, green, blue))
			neoPixel[n] = (red, green, blue)
		else:
			neoPixel[n] = (0, 0, 0)
	neoPixel.write()

def pirTriggered(pirPin):
	print("PIR: {}".format(pirPin.value()))
	mqttClient.connect()
	mqttClient.publish(PIR_TOPIC, b"{}".format(str(pirPin.value())))
	mqttClient.disconnect()

def sequenceLedPatterns(topic, msg=None):
	global neoPixel, patternId

	if type(topic) is Pin:
		if patternId>2:
			patternId=patternId+1
		else:
			patternId=1
	else:
		print("Got callback: {} {}".format(topic, msg))
		patternId=int(msg.decode())

	neoPixel = NeoPixel(Pin(2), int(LONG_LED_COUNT))
	if patternId==1:
		print("PATTERN 1")
		for i in range(LONG_LED_COUNT-100):
			for j in range(LONG_LED_COUNT-100):
				neoPixel[j] = (0, 0, 0)
			neoPixel[i % (LONG_LED_COUNT-100)] = (255, 255, 255)
			neoPixel.write()
			time.sleep_ms(25)
	elif patternId==2:
		print("PATTERN 2")
		for i in range(0, 4 * 256, 8):
			for j in range(LONG_LED_COUNT-100):
				if (i // 256) % 2 == 0:
					val = i & 0xff
				else:
					val = 255 - (i & 0xff)
				neoPixel[j] = (val, 0, 0)
			neoPixel.write()
	elif patternId==3:
		print("PATTERN 3")
	else:
		print("PATTERN ELSE")
		print(patternId)


def output_cb(topic, msg):
	print((topic.decode(), msg.decode()))
	if topic==TEMP_TOPIC:
		print("Temperature output")
		temp = float(msg.decode())

		formattedTemp = "{:.1f}".format(temp)
		print("Temp:{}".format(formattedTemp))

		#OLED Display
		display.fill(0)
		display.text("{} DegC".format(formattedTemp), 25, 25, 1)
		display.show()

		wholeFraction = (temp/SHORT_LED_COUNT)*LED_MULTIPLIER
		if(wholeFraction >= SHORT_LED_COUNT):
			wholeFraction = SHORT_LED_COUNT

		whole = int(wholeFraction)
		fraction = wholeFraction - whole
		ratio = int(255*fraction)
		
		#The wholes
		for n in range(0, whole):
			neoPixel[n] = (255, 0, 0) # set the first pixel to white

		#The fraction
		neoPixel[whole] = (ratio, 0, 255-ratio)

		#clear the rest
		for n in range(whole+1, int(SHORT_LED_COUNT)):
			neoPixel[n] = (0, 0, 0) # set the first pixel to white

		neoPixel.write()
	elif topic==PIR_TOPIC:
		print("PIR output")
		ledBluePin.value(int(msg.decode()))
	elif topic==MANUAL_TOPIC:
		print("Manual output")
		ledRedPin.value(int(msg.decode()))
	else:
		print("Unknown output")

print("Starting client type: {}".format(config.clientType))
if config.clientType=="input":
	buttonPin1 = Pin(1, Pin.IN, Pin.PULL_UP)
	buttonPin2 = Pin(3, Pin.IN, Pin.PULL_UP)
	buttonPin3 = Pin(15, Pin.IN, Pin.PULL_UP)
	buttonPin4 = Pin(13, Pin.IN, Pin.PULL_UP)
	tim = Timer(-1)
	if buttonPin1.value()==LOW:
		print("Buton 1 mode - ADC LEDS")
		display.fill(0)
		display.text("ADC LEDS Mode", 0, 25, 1)
		display.show()

		neoPixel = NeoPixel(Pin(2), int(LONG_LED_COUNT))
		tim.init(period=ADC_SAMPLE_RATE_MS, mode=Timer.PERIODIC, callback=adcSample)
	elif buttonPin2.value()==LOW:
		print("Button 2 mode - LED Patterns")
		display.fill(0)
		display.text("LED Patterns", 0, 25, 1)
		display.show()

		buttonPin2.irq(trigger=Pin.IRQ_FALLING, handler=sequenceLedPatterns)
		mqttClient.set_callback(sequenceLedPatterns)
		mqttClient.connect()
		mqttClient.subscribe(LED_PATTERN_TOPIC)
		try:
			while 1:
				mqttClient.wait_msg()
		finally:
			mqttClient.disconnect()
	elif buttonPin4.value()==LOW:
		print("Button 3 mode - PIR")
		display.fill(0)
		display.text("PIR", 0, 25, 1)
		display.show()

		pirPin.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=pirTriggered)
	else:
		print("Button ELSE mode - Temperature display")
		display.fill(0)
		display.text("Temperature", 0, 25, 1)
		display.show()

		tim.init(period=TEMPERATURE_SAMPLE_RATE_MS, mode=Timer.PERIODIC, callback=temperatureSample)

elif config.clientType=="output":

	neoPixel = NeoPixel(Pin(2), int(SHORT_LED_COUNT))

	mqttClient.set_callback(output_cb)
	mqttClient.connect()
	mqttClient.subscribe(PIR_TOPIC)
	mqttClient.subscribe(TEMP_TOPIC)
	mqttClient.subscribe(MANUAL_TOPIC)
	try:
		while 1:
			mqttClient.wait_msg()
	finally:
		mqttClient.disconnect()

else:
	print("Unknown Client Type?")
